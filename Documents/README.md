# FC_TTStatSystem

TTStatSystem is a web API developed to provide scoring and statistical data for table tennis competitions in Czech Republic.

Technologies:
+ .NET Core Web Application (.NET Core 3.1)
+ Angular
+ MS SQL
+ Rest API