﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FC_TTCore.Models;
using FC_TTStatSystem.Services;


namespace FC_TTStatSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FakeTeamsController : ControllerBase
    {

        FakeDbContext _context;

        public FakeTeamsController(FakeDbContextService contextService)
        {
            _context = contextService.DBContext;
        }

        // GET: api/FakeTeams
        [HttpGet]
        public IEnumerable<FakeTeam> Get()
        {
            return _context.FakeTeams;
        }

        // GET: api/FakeTeams/1
        [HttpGet("{id}", Name = "GetFakeTeam")]
        public FakeTeam Get(int id)
        {
            return _context.FakeTeams[id];
        }
    }
}
