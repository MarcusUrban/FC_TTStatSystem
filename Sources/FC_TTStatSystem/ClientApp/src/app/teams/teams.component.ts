import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'teams',
  templateUrl: './teams.component.html'
})
export class TeamsComponent {
  public teams: Team[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Team[]>(baseUrl + 'api/FakeTeams').subscribe(result => {
      this.teams = result;
    }, error => console.error(error));
  }
}

interface Team {
  id: number;
  fullName: string;
  address: string;
  city: string;
}
