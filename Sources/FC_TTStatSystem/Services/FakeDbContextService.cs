﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FC_TTCore.Models;

namespace FC_TTStatSystem.Services
{
    public class FakeDbContextService
    {
        public FakeDbContext DBContext { get; }

        public FakeDbContextService()
        {
            DBContext = new FakeDbContext();
        }
    }
}
